# coding:utf-8
import time
import asyncio


now = lambda: time.time()


async def do_some_work(x):  # 定义一个协程
	print("waiting:", x)

start = now()

corout = do_some_work(2)  # 定义一个协程对象
print(corout)

loop = asyncio.get_event_loop()  # 创建一个loop事件

loop.run_until_complete(corout)  # 将协程注册到事件循环，并启动事件循环

print("Time: ", now() - start)
