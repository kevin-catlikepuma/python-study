#!F:\PythonCode\all_study\env\Scripts
# coding:utf-8
import datetime
import time


def get_day_or_time():
	cur_time = datetime.datetime.now()
	print type(cur_time), cur_time  # 2018-01-15 16:19:50.795000
	print datetime.date.today()  # 2018-01-15
	print datetime.date.weekday(cur_time)
	print datetime.date.ctime(cur_time)
	print datetime.datetime.timetuple(cur_time)

	print time.time()  # 1516006955.59
	print time.localtime(time.time())
	# time.struct_time(tm_year=2018, tm_mon=1, tm_mday=15, tm_hour=17, tm_min=3, tm_sec=17, tm_wday=0, tm_yday=15, m_isdst=0)


def format_time():
	cur_time = datetime.datetime.now()
	print cur_time  # 2018-01-15 16:19:50.795000
	y_m_d_time = cur_time.strftime("%Y-%m-%d")
	print y_m_d_time
	normal_time = cur_time.strftime("%Y-%m-%d %H:%M:%S")
	print normal_time
	hour_time = datetime.datetime.now().strftime("%H")
	print hour_time
	day_time = datetime.datetime.now().strftime("%d")
	print day_time
	week_time = datetime.datetime.now().strftime("%A")
	print week_time

	std_time = datetime.datetime.now().strftime("%c")
	print std_time

	print time.strftime('%Y-%m-%d', time.localtime(time.time()))
	print time.strftime('%d', time.localtime(time.time()))
	print time.strftime('%c', time.localtime(time.time()))
	print time.strftime('%A', time.localtime(time.time()))


def time_convert():
	# 字符串转datetime
	str_time = '2014-01-08 11:59:58'
	print type(str_time)
	obj_time = datetime.datetime.strptime(str_time, "%Y-%m-%d %H:%M:%S")
	print type(obj_time), obj_time

	# datetime转字符串：
	str_time1 = datetime.datetime.strftime(obj_time, "%Y-%m-%d %H:%M:%S")
	print type(str_time1), str_time1

	# 时间戳转时间对象
	time_stamp = time.time()
	print type(time_stamp), time_stamp
	obj_time2 = datetime.datetime.fromtimestamp(time_stamp)
	print type(obj_time2), obj_time2

	# 时间结构体转时间戳
	local_time = time.localtime()
	print type(local_time), local_time
	print local_time.tm_year
	time_stamp1 = time.mktime(local_time)
	print type(time_stamp1), time_stamp1

	str_time2 = '2014-01-08 11:59:58'
	print type(str_time2), str_time2
	struct_time = time.strptime(str_time2, "%Y-%m-%d %H:%M:%S")
	print type(struct_time), struct_time


# 时间字符串为参数
def time_cmp(str_time1, str_time2):
	struct_time1 = time.strptime(str_time1, "%Y-%m-%d %H:%M:%S")
	stamp_time1 = time.mktime(struct_time1)

	struct_time2 = time.strptime(str_time2, "%Y-%m-%d %H:%M:%S")
	stamp_time2 = time.mktime(struct_time2)

	if stamp_time1 >= stamp_time2:
		return True
	else:
		return False


if __name__ == "__main__":
	time_convert()


"""
%a 星期几的简写
%A 星期几的全称
%b 月分的简写
%B 月份的全称
%c 标准的日期的时间串
%C 年份的后两位数字
%d 十进制表示的每月的第几天
%D 月/天/年
%e 在两字符域中，十进制表示的每月的第几天
%F 年-月-日
%g 年份的后两位数字，使用基于周的年
%G 年分，使用基于周的年
%h 简写的月份名
%H 24小时制的小时
%I 12小时制的小时
%j 十进制表示的每年的第几天
%m 十进制表示的月份
%M 十时制表示的分钟数
%n 新行符
%p 本地的AM或PM的等价显示
%r 12小时的时间
%R 显示小时和分钟：hh:mm
%S 十进制的秒数
%t 水平制表符
%T 显示时分秒：hh:mm:ss
%u 每周的第几天，星期一为第一天 （值从0到6，星期一为0）
%U 第年的第几周，把星期日做为第一天（值从0到53）
%V 每年的第几周，使用基于周的年
%w 十进制表示的星期几（值从0到6，星期天为0）
%W 每年的第几周，把星期一做为第一天（值从0到53）
%x 标准的日期串
%X 标准的时间串
%y 不带世纪的十进制年份（值从0到99）
%Y 带世纪部分的十制年份
%z，%Z 时区名称，如果不能得到时区名称则返回空字符。
%% 百分号
"""